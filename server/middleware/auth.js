import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';

dotenv.config();

const auth = async (req, res, next) => {
    try {
        //check whether token is valid for certain action
        const token = req.headers.authorization.split(" ")[1];
        const isCustomAuth = token.length <500;

        let decodedData;

        if(token && isCustomAuth) {
            decodedData = jwt.verify(token,process.env.JWT_ACC_ACTIVATE); // give username and id to us
        
            req.userId = decodedData?.id; //get user's id
        } else {
            decodedData = jwt.decode(token);

            req.userId = decodedData?.sub; //get google id
        }

        next(); //can do the action if the auth middleware verifies the token

    } catch (error) {
        console.log(error);
    }
}

export default auth; // can be used in the routes for action that required login