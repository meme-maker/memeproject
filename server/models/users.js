import mongoose from 'mongoose';

//create database in mongodb for user records

const userSchema = mongoose.Schema({
    name: {type: String, required: true},
    email: {type: String, required: true},
    password: {type: String, required: true},
    id: {type: String}
});

const user = mongoose.model('User', userSchema);

export default user;