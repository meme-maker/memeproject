import mongoose from 'mongoose';

//create database in mongodb for meme records

const postSchema = mongoose.Schema({
    title: String,
    description: String,
    creator: String,
    creatorname: String,
    tags: [String],
    selectedFile: String,
    isDraft: {
        type: Boolean,
        default: 'true'
    },
    isPrivate: {
        type: Boolean,
        default: 'false'
    },
    likes: { type: [String], default: [] },
    comments: { type: [String], default: [] },
    createdAt: {
        type: Date,
        default: new Date()
    },
});

const PostMessage = mongoose.model('PostMessage', postSchema);

export default PostMessage;