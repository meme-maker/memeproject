import bcrypt from 'bcryptjs'; //for encryption
import jwt from 'jsonwebtoken'; //how long stay login
import dotenv from 'dotenv';

import user from '../models/users.js';

dotenv.config();

export const signin = async (req, res) => {
    const {email, password} = req.body;
    try {
        const existingUser = await user.findOne({email});
        
        if(!existingUser) return res.status(404).json({message: "User doesn't exist."});

        const isPasswordCorrect = await bcrypt.compare(password, existingUser.password); //to compare encrypted password with the inputted one

        if(!isPasswordCorrect) return res.status(400).json({message:"Invalid credentials"});

        const token = jwt.sign({email: existingUser.email, id: existingUser._id}, process.env.JWT_ACC_ACTIVATE, {expiresIn: "1h"});
    
        res.status(200).json({result: existingUser, token});
    } catch (error) {
        res.status(500).json({message: "Something went wrong"});

    }
}

export const signup = async (req, res) => {
    const {email, password, confirmPassword, firstName, lastName} = req.body;
    try {
        //check existing user first then create if not existing
        const existingUser = await user.findOne({email});
        
        if(existingUser) return res.status(400).json({message: "User already exists."});

        if(password != confirmPassword) return res.status(400).json({message: "Passwords don't match."});

        const hashedPassword = await bcrypt.hash(password, 12); // 12 means the length of hashed characters

        const result = await user.create ({email, password: hashedPassword, name: `${firstName} ${lastName}`});

        const token = jwt.sign({email: result.email, id: result._id}, process.env.JWT_ACC_ACTIVATE, {expiresIn: "1h"});
    
        res.status(200).json({result: result, token});
    } catch (error) {
        res.status(500).json({message: "Something went wrong"});

    }
}