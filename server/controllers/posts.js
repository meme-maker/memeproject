import express from 'express';
import mongoose from 'mongoose';

import PostMessage from '../models/postMessages.js';

const router = express.Router();

export const getPosts = async (req, res) => {
    try {
        //only get posts which are allowed to be view to public, with default sorting sequence as desc based on creation date
        const postMessages = await PostMessage.find({ isPrivate: 'false', isDraft: 'false' }).sort({createdAt: -1});
        
        res.status(200).json(postMessages);

    } catch (error) {
        res.status(404).json({ message: error.message });

    }
}

export const getPostsBySearch = async (req, res) => {
    const {searchQuery, filter, sort, sortnum} = req.query;
    try {
        //storing searchquery into database items with no-case limitation
        const title = new RegExp(searchQuery, 'i');
        const description = new RegExp(searchQuery, 'i');
        const tags = new RegExp(searchQuery, 'i');
        const creatorname = new RegExp(searchQuery, 'i');
        const comments = new RegExp(searchQuery, 'i');  

        //set up variables to be updated based on search request
        var filteritem = {$or: [{title}, { description},{ tags},{ creatorname}, {comments}]};
        var sortitem = {createdAt: sortnum};

        if (filter == "Title") {
            filteritem = {title}
        } 
        else if (filter == "Description"){
            filteritem = {description}
        }
        else if (filter == "Creator"){
            filteritem = {creatorname}
        }
        else if (filter == "Tags"){
            filteritem = {tags}
        }
        else if (filter == "Comments"){
            filteritem = {comments}
        } else {
            filteritem = {$or: [{title}, { description},{ tags},{ creatorname}, {comments}]}
        };

        if (sort == "Title") {
            sortitem = {title: sortnum};
        }
        else if (sort == "Creator"){
            sortitem = {creatorname: sortnum};
        } else {
            sortitem = {createdAt: sortnum};
        };

        //only get posts which are allowed to be view to public, with sorting sequence based on search request; if nothing input in searchquery will be considered as searching all data

        const postMessages = (searchQuery == "none")? await PostMessage.find({ isPrivate: 'false', isDraft: 'false' }).sort(sortitem) : await PostMessage.find({$and: [{ isPrivate: 'false', isDraft: 'false' },filteritem]}).sort(sortitem);

        res.status(200).json({data: postMessages});

    } catch (error) {
        res.status(404).json({ message: error.message });

    }
}

export const getPost = async (req, res) => {
    const { id } = req.params;

    try {
        //get individual post based on specific id to display details
        const post = await PostMessage.findById(id);

        res.status(200).json(post);

    } catch (error) {
        res.status(404).json({ message: error.message });

    }
}

export const getaccountPosts = async (req, res) => {
    try {
        //only get posts which are created by logined-user only, with default sorting sequence as desc based on creation date
        
        const posts = await PostMessage.find({ creator: req.userId }).sort({createdAt: -1});
        res.status(200).json(posts);

    } catch (error) {
        res.status(404).json({ message: error.message });

    }
}

export const getaccountPostsBySearch = async (req, res) => {
    const {searchQuery, filter, sort, sortnum} = req.query;
    try {
        //storing searchquery into database items with no-case limitation

        const title = new RegExp(searchQuery, 'i');
        const description = new RegExp(searchQuery, 'i');
        const tags = new RegExp(searchQuery, 'i');
        const creatorname = new RegExp(searchQuery, 'i');
        const comments = new RegExp(searchQuery, 'i');  

        //set up variables to be updated based on search request

        var filteritem = {$or: [{title}, { description},{ tags},{ creatorname}, {comments}]};
        var sortitem = {createdAt: sortnum};

        if (filter == "Title") {
            filteritem = {title}
        } 
        else if (filter == "Description"){
            filteritem = {description}
        }
        else if (filter == "Tags"){
            filteritem = {tags}
        }
        else if (filter == "Comments"){
            filteritem = {comments}
        } else {
            filteritem = {$or: [{title}, { description},{ tags},{ creatorname}, {comments}]}
        }

        if (sort == "Title") {
            sortitem = {title: sortnum};
        } else {
            sortitem = {createdAt: sortnum};
        }
       
        //only get posts which are created by logined-user only, with sorting sequence based on search request; if nothing input in searchquery will be considered as searching all data

        const postMessages = (searchQuery == "none")? await PostMessage.find({ creator: req.userId }).sort(sortitem) : await PostMessage.find({$and: [{ creator: req.userId },filteritem]}).sort(sortitem);

        res.status(200).json({data: postMessages});

    } catch (error) {
        res.status(404).json({ message: error.message });
    }
}

export const createPost = async (req, res) => {
    const post = req.body;
    
    // get data from frontend and stored in database variable to be passed to mongodb
    const newPost = new PostMessage({ ...post, creator: req.userId, createdAt: new Date().toISOString() });
    try {
        // takes time for the data to be saved and passed to mongodb
        await newPost.save();

        res.status(201).json(newPost);
    } catch (error) {
        res.status(409).json({ message: error.message });
    }

}

export const updatePost = async (req, res) => {
    const { id } = req.params;
    const post = req.body;

    if (!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send(`No post with id: ${id}`);

    const updatedPost = await PostMessage.findByIdAndUpdate(id, { ...post, _id: id }, { new: true });

    res.json(updatedPost);
}

export const deletePost = async (req, res) => {
    const { id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send(`No post with id: ${id}`);

    await PostMessage.findByIdAndRemove(id);

    res.json({ message: "Post deleted successfully." });
}

export const likePost = async (req, res) => {
    const { id } = req.params;

    if (!req.userId) {
        return res.json({ message: "Unauthenticated" });
    }

    if (!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send(`No post with id: ${id}`);

    const post = await PostMessage.findById(id);

    const index = post.likes.findIndex((id) => id === String(req.userId));

    if (index === -1) {
        post.likes.push(req.userId);
    } else {
        post.likes = post.likes.filter((id) => id !== String(req.userId));
    }

    const updatedPost = await PostMessage.findByIdAndUpdate(id, post, { new: true });

    res.status(200).json(updatedPost);
}

export const commentPost = async (req, res) => {
    const { id } = req.params;
    const { value } = req.body;

    const post = await PostMessage.findById(id);

    post.comments.push(value);

    const updatedPost = await PostMessage.findByIdAndUpdate(id, post, { new: true });

    res.json(updatedPost);
}



export default router;
