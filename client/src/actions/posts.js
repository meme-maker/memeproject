import { START_LOADING, END_LOADING, FETCH_ALL, FETCH_POST,  FETCH_ACCOUNTALL, CREATE, UPDATE, DELETE, LIKE, COMMENT, FETCH_BY_SEARCH, FETCH_ACCOUNT_BY_SEARCH} from '../constants/actionTypes';
import * as api from '../api/index';

//functions that create actions

export const getPost = (id) => async (dispatch) => {
  try {
    dispatch({ type: START_LOADING });

    const { data } = await api.fetchPost(id);

    dispatch({ type: FETCH_POST, payload: data });
  } catch (error) {
    console.log(error);
  }
};

export const getPosts = () => async (dispatch) => {
  try {
    dispatch({ type: START_LOADING });

    const { data } = await api.fetchPosts();

    dispatch({ type: FETCH_ALL, payload: data });
    dispatch({type: END_LOADING});
  } catch (error) {
    console.log(error.message);
  }
}

export const getPostsBySearch = (searchQuery) => async (dispatch) => {
  try {
    dispatch({ type: START_LOADING });
    const { data: { data } } = await api.fetchPostsBySearch(searchQuery);
    dispatch({ type: FETCH_BY_SEARCH, payload: { data } });
    dispatch({ type: END_LOADING });
  } catch (error) {
    console.log(error);
  }
};

export const createPost = (post, navigate) => async (dispatch) => {
  try {
    dispatch({ type: START_LOADING });
    const { data } = await api.createPost(post);

    dispatch({ type: CREATE, payload: data });

    navigate.push(`/posts/${data._id}`);
  } catch (error) {
    console.log(error);
  }
};

export const getaccountPosts = () => async (dispatch) => {
  try {

    dispatch({ type: START_LOADING });
    const { data } = await api.fetchaccountPosts();

    dispatch({ type: FETCH_ACCOUNTALL, payload: data });
    dispatch({type: END_LOADING});
  } catch (error) {
    console.log(error);
  }
}

export const getaccountPostsBySearch = (searchQuery) => async (dispatch) => {
  try {
    dispatch({ type: START_LOADING });
    const { data: { data } } = await api.fetchaccountPostsBySearch(searchQuery);
    dispatch({ type: FETCH_ACCOUNT_BY_SEARCH, payload: { data } });
    dispatch({ type: END_LOADING });
  } catch (error) {
    console.log(error);
  }
};

export const updatePost = (id, post) => async (dispatch) =>{
  try {
    const {data} = await api.updatePost(id, post); //updated post

    dispatch({type: UPDATE, payload: data});
  } catch (error) {
    console.log(error);
  }
}

export const deletePost = (id) => async (dispatch) =>{
  try {
    await api.deletePost(id);

    dispatch({type: DELETE, payload: id});
  } catch (error) {
    console.log(error);
  }
}

export const deleteaccountPost = (id) => async (dispatch) =>{
  try {
    await api.deleteaccountPost(id);

    dispatch({type: DELETE, payload: id});
  } catch (error) {
    console.log(error);
  }
}


export const likePost = (id) => async (dispatch) => {
  const user = JSON.parse(localStorage.getItem('profile'));

  try {
    const { data } = await api.likePost(id, user?.token);

    dispatch({ type: LIKE, payload: data });
  } catch (error) {
    console.log(error);
  }
};


export const commentPost = (value, id) => async (dispatch) => {
  try {
    const { data } = await api.comment(value, id);

    dispatch({ type: COMMENT, payload: data });

    return data.comments;
  } catch (error) {
    console.log(error);
  }
};
