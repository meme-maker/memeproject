import React, { useEffect, useState } from 'react';
import { Link, useNavigate, useLocation } from 'react-router-dom';
import { AppBar, Avatar, Button, Toolbar, Typography } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import decode from 'jwt-decode';
import "./style.scss";



const Navbar = () => {
    const [user, setUser] = useState(JSON.parse(localStorage.getItem('profile')));
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const location = useLocation();

    const logout = () => {
        dispatch({ type: "LOGOUT" });

        navigate('/auth');

        setUser(null);
    };

    useEffect(() => {
        const token = user?.token;

        if (token) {
            const decodedToken = decode(token);

            if (decodedToken.exp * 1000 < new Date().getTime()) logout();
        }

        setUser(JSON.parse(localStorage.getItem('profile')));
    }, [location]);

    return (

        <header class="Header">
            {/* Logo */}
            <div id="logo" >
                <a href="/">
                    <img className="logo"
                        src="https://memeland.co/images/tild6533-3137-4638-a237-356330323337__memeland_large-logo_.svg"
                        alt="Meme Land"
                        loading="lazy"
                    // height= "41px"
                    // width= "151px"
                    // margin-right= "10px"
                    />
                </a>
            </div>

            {/* Navigation */}
            {user ? (
                <div id="navigation" class="Navigation">
                    <nav>
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li><a href="/account">My Meme</a></li>
                            <li><a href="/create">Create New</a></li>
                            <li><a href="/upload">Share Meme</a></li>
                        </ul>
                    </nav>
                </div>
            ) : (
                <div id="navigation" className="Navigation">
                    <nav>
                        <ul>
                            <li><a href="/">Home</a></li>
                        </ul>
                    </nav>
                </div>
            )}



            {/* UserProfile */}
            {user ? (
                <div class="UserProfile">
                    <div class="User">
                        <div className="name">{user.result.name}  </div>
                        <Avatar className="image" alt={user.result.name} src={user.result.imageUrl}>{user.result.name.charAt(0)}</Avatar>
                        <Button variant="contained" className="logout" onClick={logout}>Logout</Button>
                    </div>
                </div>
            ) : (
                <div class="UserProfile">
                        <Button variant="contained" className="logout" component={Link} to="/auth">Sign In</Button>
                </div>

            )}
        </header>





    );
};

export default Navbar;
