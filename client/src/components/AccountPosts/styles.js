import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  mainContainer: {
    display: 'flex',
    alignItems: 'center',
  },
  smMargin: {
    margin: theme.spacing(1),
  },
  actionDiv: {
    textAlign: 'center',
  },
  centerP: {
    textAlign: 'center',
    color: 'white',
    fontWeight: 'bold',
    fontSize: '23px',
  }
}));