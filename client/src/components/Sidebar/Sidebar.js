import React, { useState, useEffect } from 'react';
import { Paper, Button, TextField, FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';
import { getPosts, getPostsBySearch } from '../../actions/posts.js';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import './sidStyle.scss';
import useStyles from './styles';


const Sidebar = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const navigate = useNavigate();


  //search, filter, sort
  const [search, setSearch] = useState('');
  const [filteritem, setFilteritem] = React.useState(["All", "Title", "Description", "Tags", "Creator", "Comments"]);
  const [filter, setFilter] = useState("All");
  let [likesnumber, setLikesnumber] = useState('');
  const [likesrange, setLikerange] = React.useState(["Greater than", "Less than","Greater than or equal to","Less than or equal to", "Equals"]);
  const [likesr, setLikesr] = useState("Greater than or equal to");
  const [sortseq, setSortseq] = React.useState(["DESC - Z-A/ Latest", "ASC - A-Z/ Oldest"]);
  const [sortorder, setSortorder] = useState("DESC - Z-A/ Latest");
  const [sortitem, setSortitem] = React.useState(["Title", "Creator", "Created Date"]);
  const [sort, setSort] = useState("Created Date");

  //for passing search request and stores likes request temporarily locally

  const handleSearchSubmit = async (e) => {

    e.preventDefault();

    const sortnum = (sortorder == "DESC - Z-A/ Latest" ? -1 : 1);

    if (likesnumber=='') {
      likesnumber =0
    }

    if (search.trim()) {
        dispatch(getPostsBySearch({ search, filter, sort, sortnum }));
        sessionStorage.setItem("likesr", likesr);
        sessionStorage.setItem("likesnumber", likesnumber);
        navigate(`/posts/search?searchQuery=${search || 'none'}`);
    } else if (search == ''){
        dispatch(getPostsBySearch({ search:'', filter, sort, sortnum }));
        sessionStorage.setItem("likesr", likesr);
        sessionStorage.setItem("likesnumber", likesnumber);
        navigate(`/posts/search?searchQuery=${search || 'none'}`);
    } else {
      dispatch(getPosts())
      sessionStorage.setItem("likesr", "Greater than or equal to");
      sessionStorage.setItem("likesnumber", 0);
      navigate('/posts');
    }
  };

  //for all dropdown menu
  const handlefilterChange = (e) => {
    setFilter(e.target.value);
  };

  const handlesortorderChange = (e) => {
    setSortorder(e.target.value);
  };

  const handlesortitemChange = (e) => {
    setSort(e.target.value);
  };

  const handlelikesrChange = (e) => {
    setLikesr(e.target.value);
  };

  const styles = {
    control: base => ({
      ...base,
      fontColor: "#fffff"
    }),
    menu: base => ({
      ...base,
      fontColor: "#fffff"
    })
  };

  const handleKeyPress = (e) => {
    if (e.keyCode === 13) {
      handleSearchSubmit();
    }
  };

  return (
    <div>
      <div class="app">
        <div class="wrapper">
          <div class="left-side">
            <form autoComplete="off" noValidate onSubmit={handleSearchSubmit}>


              {/* Search */}
              <div class="side-wrapper">
                <div class="side-title">Search</div>
                <div class="side-menu">
                  <div class="search-bar">
                    <input name="search" placeholder="Search" variant="outlined" fullWidth value={search} onKeyDown={handleKeyPress} onChange={(e) => setSearch(e.target.value || '')} />
                  </div>
                </div>
              </div>

              {/* Filter */}
              <div class="side-wrapper">
                <div class="side-title">Filter based on</div>
                <div class="side-menu">
                  <a>
                    <FormControl>
                      <Select
                        classes={{
                          root: classes.whiteColor,
                          icon: classes.whiteColor
                        }}
                        disableUnderline
                        value={filter}
                        onChange={handlefilterChange}
                        inputProps={{
                          name: "filter",
                          id: "filteritem"
                        }}
                      >
                        {filteritem.map((value, index) => {
                          return <MenuItem value={value}>{value}</MenuItem>;
                        })}
                      </Select>
                    </FormControl>
                  </a>

                </div>
              </div>

              {/* Likes Range */}
              <div class="side-wrapper">
                <div class="side-title">Likes Range</div>
                <div class="side-menu">
                  <a>
                    <FormControl>
                      <Select
                        classes={{
                          root: classes.whiteColor,
                          icon: classes.whiteColor
                        }}
                        disableUnderline
                        value={likesr}
                        onChange={handlelikesrChange}
                        inputProps={{
                          name: "likesr",
                          id: "likesrange"
                        }}
                      >
                        {likesrange.map((value, index) => {
                          return <MenuItem value={value}>{value}</MenuItem>;
                        })}
                      </Select>
                    </FormControl>
                  </a>

                </div>
              </div>

              {/* Likes  number */}
              <div class="side-wrapper">
                <div class="side-title"></div>
                <div class="side-menu search-bar">
                    <input name="likesnumber" placeholder="Likes Count" variant="outlined" fullWidth value={likesnumber} onChange={(e) => setLikesnumber(e.target.value || '')} />
                </div>
              </div>


              {/* Sort Item */}
              <div class="side-wrapper">
                <div class="side-title">Sort based on</div>
                <div class="side-menu">
                  <a>
                    <FormControl>
                      <Select
                        classes={{
                          root: classes.whiteColor,
                          icon: classes.whiteColor
                        }}
                        disableUnderline
                        value={sort}
                        onChange={handlesortitemChange}
                        inputProps={{
                          name: "sort",
                          id: "sortitem"
                        }}
                      >
                        {sortitem.map((value, index) => {
                          return <MenuItem value={value}>{value}</MenuItem>;
                        })}
                      </Select>
                    </FormControl>
                  </a>
                </div>
              </div>

              {/* Sort Sequence */}
              <div class="side-wrapper">
                <div class="side-title">Sort Sequence</div>
                <div class="side-menu">
                  <a>
                    <FormControl>
                      <Select
                        classes={{
                          root: classes.whiteColor,
                          icon: classes.whiteColor
                        }}
                        disableUnderline
                        value={sortorder}
                        onChange={handlesortorderChange}
                        inputProps={{
                          name: "sortorder",
                          id: "sortseq"
                        }}
                      >
                        {sortseq.map((value, index) => {
                          return <MenuItem value={value}>{value}</MenuItem>;
                        })}
                      </Select>
                    </FormControl>
                  </a>
                </div>
              </div>


              {/* Search Button */}
              <div class="side-wrapper">
                <div class="side-menu like-content">
                  <Button class="btn-secondary like-review" variant="contained" color="primary" size="large" type="submitsearch" fullWidth><strong>Submit Search</strong></Button>
                </div>
              </div>



            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Sidebar;


