import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
    },
  },
  paper: {
    padding: '20px',
    // margin: '50px',
    top: '13.3%',
    position: 'sticky',
    width: '70%',
    marginLeft: '23%',
    display: 'inline-block',
    borderRadius: '10px',
    float: 'top',
    stickyTop: true,
  },
  form: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
  fileInput: {
    width: '97%',
    margin: '10px 0',
  },
  buttonSubmit: {
    marginBottom: 10,
  },
  
}));