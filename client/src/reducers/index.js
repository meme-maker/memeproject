import { combineReducers } from 'redux';

//for both elements from posts and auth

import posts from './posts';
import auth from './auth';

export const reducers= combineReducers({posts, auth});